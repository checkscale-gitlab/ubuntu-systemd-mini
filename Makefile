# ubuntu-systemd-mini

all: help

.PHONY: all help build pull push shell

help:
	@echo "Available targets:"
	@echo
	@echo "  build      build and tag Docker image"
	@echo "  manifest   generate a list of packages & versions included in Docker image"
	@echo "  pull       pull Docker image"
	@echo "  push       push Docker image"
	@echo "  shell      run a shell in Docker image"
	@echo


CUSTOM_VERSION?=1.0
UBUNTU_VERSION?=18.04

DOCKER_IMAGE:=ubuntu-systemd-mini
DOCKER_REGISTRY_URL?=registry.gitlab.com/docking/
DOCKER_TAG?=$(CUSTOM_VERSION)-$(UBUNTU_VERSION)

build:
	docker build \
		--build-arg DOCKER_REGISTRY_URL=$(DOCKER_REGISTRY_URL) \
		--build-arg CUSTOM_VERSION=$(CUSTOM_VERSION) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		--tag $(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(if $(DOCKER_NO_CACHE),--no-cache,) \
		.

manifest:
	docker run \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		dpkg-query --show --showformat='$${Package}=$${Version}\n' | sort \
		> ./Manifest.txt

pull:
	docker pull \
		$(DOCKER_REGISTRY_URL)$(DOCKER_IMAGE):$(DOCKER_TAG)
	docker tag \
		$(DOCKER_REGISTRY_URL)$(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(DOCKER_IMAGE):$(DOCKER_TAG)

push:
	docker tag \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(DOCKER_REGISTRY_URL)$(DOCKER_IMAGE):$(DOCKER_TAG)
	docker push \
		$(DOCKER_REGISTRY_URL)$(DOCKER_IMAGE):$(DOCKER_TAG)

shell:
	docker run \
		--interactive --tty \
		$(DOCKER_IMAGE):$(DOCKER_TAG) \
		bash
