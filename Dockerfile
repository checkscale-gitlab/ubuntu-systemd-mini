# ubuntu-systemd-mini

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=1.0
ARG UBUNTU_VERSION=18.04


FROM ${DOCKER_REGISTRY_URL}ubuntu:${CUSTOM_VERSION}-${UBUNTU_VERSION} AS ubuntu-systemd-mini

WORKDIR /root

RUN \
	rm -f /usr/sbin/policy-rc.d ; ln -sf /bin/true /usr/sbin/policy-rc.d

RUN \
	apt-get -q -y update ; \
	apt-get -q -y install \
		systemd \
		systemd-sysv \
	; \
	apt-get -q -y clean ; \
	find /var/lib/apt/lists/ -type f -delete

RUN \
	systemctl disable \
		apt-daily.timer \
		apt-daily-upgrade.timer \
		fstrim.timer \
		getty@tty1.service \
		motd-news.timer \
		networkd-dispatcher.service \
		ondemand.service \
		remote-fs.target \
		systemd-resolved.service \
		systemd-timesyncd.service \
	; \
	systemctl mask \
		dbus.service \
		dbus.socket \
		getty.target \
		systemd-ask-password-wall.path \
		systemd-initctl.socket \
		systemd-logind.service \
		systemd-remount-fs.service \
		systemd-update-utmp-runlevel.service \
		systemd-user-sessions.service \
	; \
	systemctl set-default multi-user.target

RUN \
	sed -i \
		-e 's|^.\(Storage\)=.*|\1=volatile|' \
		-e 's|^.\(RuntimeMaxUse\)=.*|\1=100M|' \
		-e 's|^.\(ForwardToConsole\)=.*|\1=yes|' \
		/etc/systemd/journald.conf

STOPSIGNAL SIGRTMIN+3

VOLUME /run /run/lock /tmp /sys/fs/cgroup

CMD exec /sbin/init
